# Ansible Execution Enviroment

# Cool Stuff right here -

Steps I took to install my AWX setup

##### At the moment, there is no major security concern here, so it is simple to set up.

```bash

sudo systemctl disable firewalld --now
sudo dnf install podman -y
alias docker=podman
sudo vi /etc/sysconfig/selinux
-- Configure SELINUX=disabled
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
  
sudo firewall-cmd --zone=public --add-masquerade --permanent
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --reload
sudo dnf install python3.9 -y  

sudo ln -s /usr/bin/python3 /usr/bin/python && sudo ln -s /usr/bin/pip3 /usr/bin/pip
sudo alternatives --set python /usr/bin/python3.9
  
Reboot!
shutdown -r now
```

##### Install K3S and make the /etc/rancher/k3s/k3s.yaml readable by non-root users

```
curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644
```

##### Lets get the AWX Operator.
  
```
git clone https://github.com/ansible/awx-operator.git
cd awx-operator
git checkout 0.14.0
export NAMESPACE=awx
make deploy
```


##### Build and Push the Container Image that is used for the Execution Enviroment

##### Clone yeta-ansible-exec-evn.git and install requirements

```bash
cd ..
git clone git@gitlab.com:vcappuccio/yeta-ansible-exec-evn.git
cd yeta-ansible-exec-evn/
pip3 install -r requirements.txt
```

##### Create the Ansible EE Container Image

 ```bash
cd Builder/
AWX_HOST="awx.devel.kpn"

openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -out ./base/tls.crt -keyout ./base/tls.key -subj "/CN=${AWX_HOST}/O=${AWX_HOST}" -addext "subjectAltName = DNS:${AWX_HOST}"
sudo mkdir -p /data/postgres
sudo mkdir -p /data/projects
sudo chown 1000:0 /data/projects

kubectl apply -k base

At this point of time you should see

namespace/awx configured
secret/awx-admin-password created
secret/awx-postgres-configuration created
secret/awx-secret-tls created
persistentvolume/awx-postgres-volume created
persistentvolume/awx-projects-volume created
persistentvolumeclaim/awx-projects-claim created
awx.awx.ansible.com/awx created


[neteng@reddy Builder]$ kubectl get events
LAST SEEN TYPE REASON OBJECT MESSAGE
15m Normal Starting node/reddy.kpn Starting kubelet.
15m Warning InvalidDiskCapacity node/reddy.kpn invalid capacity 0 on image filesystem
15m Normal NodeAllocatableEnforced node/reddy.kpn Updated Node Allocatable limit across pods
15m Normal NodeHasSufficientMemory node/reddy.kpn Node reddy.kpn status is now: NodeHasSufficientMemory
15m Normal NodeHasNoDiskPressure node/reddy.kpn Node reddy.kpn status is now: NodeHasNoDiskPressure
15m Normal NodeHasSufficientPID node/reddy.kpn Node reddy.kpn status is now: NodeHasSufficientPID
15m Normal Starting node/reddy.kpn Starting kube-proxy
15m Normal Synced node/reddy.kpn Node synced successfully
15m Normal NodeReady node/reddy.kpn Node reddy.kpn status is now: NodeReady
15m Normal RegisteredNode node/reddy.kpn Node reddy.kpn event: Registered Node reddy.kpn in Controller

Complete! The build context can be found at: /home/neteng/yeta-ansible-exec-evn/Builder/context

[neteng@reddy Builder]$ podman images
REPOSITORY TAG IMAGE ID CREATED SIZE
registry.gitlab.com/vcappuccio/yeta-ansible-exec-evn latest 35324c25b926 4 minutes ago 1.36 GB. <<<< here >>>>
<none> <none> 322d839d2b0b 8 minutes ago 1.29 GB. <<< Images Generated during the build>>>
<none> <none> 27fb45cb1960 14 minutes ago 712 MB
quay.io/ansible/ansible-runner stable-2.10-devel dcba56c0c0c9 2 hours ago 644 MB
quay.io/ansible/ansible-builder latest 8e8cbed25d34 18 hours ago 538 MB


now this boy is ready to be pushed:

ansible-builder build --tag registry.gitlab.com/vcappuccio/yeta-ansible-exec-evn --verbosity 3
docker push registry.gitlab.com/vcappuccio/yeta-ansible-exec-evn:latest
```
    
##### Some Kubernetes commands  

```bash

Watch the build log for awx-operator:
kubectl -n awx logs -f deployments/awx-operator-controller-manager -c manager

kubectl -n awx get awx,all,ingress,secrets
kubectl get pods -A
kubectl -n awx get all -n awx
kubectl cluster-info
kubectl version
kubectl get deployments -n awx
export POD_NAME=$(kubectl -n awx get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
kubectl get pods -n $NAMESPACE | grep manager
kubectl get events

#So we don't have to keep repeating -n $NAMESPACE, let's set the current namespace for kubectl:
#kubectl config set-context --current --namespace=$NAMESPACE
#or a simple alias if you get permisssion denied
```

### Links

https://github.com/kurokobo/awx-on-k3s

https://debugfactor.com/how-to-install-ansible-awx-in-centos-8/

