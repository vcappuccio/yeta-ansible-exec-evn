## Commands:

```

AWX_HOST="awx.devel.kpn"
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -out ./base/tls.crt -keyout ./base/tls.key -subj "/CN=${AWX_HOST}/O=${AWX_HOST}" -addext "subjectAltName = DNS:${AWX_HOST}"

sudo mkdir -p /data/postgres
sudo mkdir -p /data/projects
sudo chown 1000:0 /data/projects

kubectl apply -k base

ansible-builder build --tag  registry.gitlab.com/vcappuccio/yeta-ansible-exec-evn  --verbosity 3

Wait for it to finish
then:
docker push registry.gitlab.com/vcappuccio/yeta-ansible-exec-evn:latest
```


## Some handy commands for kubectl:

```
kubectl -n awx logs -f deployments/awx-operator-controller-manager -c manager
kubectl -n awx get awx,all,ingress,secrets
kubectl  get pods -A
kubectl -n awx get all

More Kube commands:
kubectl cluster-info
kubectl version
kubectl get deployments -n awx
export POD_NAME=$(kubectl -n awx get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
kubectl get pods -n $NAMESPACE | grep manager

#So we don't have to keep repeating -n $NAMESPACE, let's set the current namespace for kubectl:
#kubectl config set-context --current --namespace=$NAMESPACE
#or a simple alias if you get permisssion denied

To get the IP:
kubectl -n awx get awx,all,ingress,secrets
kubectl get events
kubectl -n awx get all

podman images
podman rmi imagesha -f

```
 
